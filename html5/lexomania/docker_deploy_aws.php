
#https://docs.aws.amazon.com/aws-sdk-php/v3/api/class-Aws.Ecr.EcrClient.html

$result = $client->batchCheckLayerAvailability([
    'layerDigests' => ['<string>', ...], // REQUIRED
    'registryId' => '<string>',
    'repositoryName' => '<string>', // REQUIRED
]);

###TODO Create lexo registry


###List button
###TODO List all images in lexo registry
###https://docs.aws.amazon.com/aws-sdk-php/v3/api/
###https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-ecr-2015-09-21.html
$result = $client->listImages([
    'repositoryName' => 'ubuntu',
]);

$result = $client->listImages([
    'filter' => [
        'tagStatus' => 'TAGGED|UNTAGGED',
    ],
    'maxResults' => <integer>,
    'nextToken' => '<string>',
    'registryId' => '<string>',
    'repositoryName' => '<string>', // REQUIRED
]);


#https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-ecs-2014-11-13.html#runtask
$result = $client->listTasks([
    'cluster' => 'default',
    'containerInstance' => 'f6bbb147-5370-4ace-8c73-c7181ded911f',
]);





#####start button
#####TODO registertaskdefinition
$result = $client->registerTaskDefinition([
    'containerDefinitions' => [ // REQUIRED
        [
            'command' => ['<string>', ...],
            'cpu' => <integer>,
            'disableNetworking' => true || false,
            'dnsSearchDomains' => ['<string>', ...],
            'dnsServers' => ['<string>', ...],
            'dockerLabels' => ['<string>', ...],
            'dockerSecurityOptions' => ['<string>', ...],
            'entryPoint' => ['<string>', ...],
            'environment' => [
                [
                    'name' => '<string>',
                    'value' => '<string>',
                ],
                // ...
            ],
            'essential' => true || false,
            'extraHosts' => [
                [
                    'hostname' => '<string>', // REQUIRED
                    'ipAddress' => '<string>', // REQUIRED
                ],
                // ...
            ],
            'healthCheck' => [
                'command' => ['<string>', ...], // REQUIRED
                'interval' => <integer>,
                'retries' => <integer>,
                'startPeriod' => <integer>,
                'timeout' => <integer>,
            ],
            'hostname' => '<string>',
            'image' => '<string>',
            'links' => ['<string>', ...],
            'linuxParameters' => [
                'capabilities' => [
                    'add' => ['<string>', ...],
                    'drop' => ['<string>', ...],
                ],
                'devices' => [
                    [
                        'containerPath' => '<string>',
                        'hostPath' => '<string>', // REQUIRED
                        'permissions' => ['<string>', ...],
                    ],
                    // ...
                ],
                'initProcessEnabled' => true || false,
                'sharedMemorySize' => <integer>,
                'tmpfs' => [
                    [
                        'containerPath' => '<string>', // REQUIRED
                        'mountOptions' => ['<string>', ...],
                        'size' => <integer>, // REQUIRED
                    ],
                    // ...
                ],
            ],
            'logConfiguration' => [
                'logDriver' => 'json-file|syslog|journald|gelf|fluentd|awslogs|splunk', // REQUIRED
                'options' => ['<string>', ...],
            ],
            'memory' => <integer>,
            'memoryReservation' => <integer>,
            'mountPoints' => [
                [
                    'containerPath' => '<string>',
                    'readOnly' => true || false,
                    'sourceVolume' => '<string>',
                ],
                // ...
            ],
            'name' => '<string>',
            'portMappings' => [
                [
                    'containerPort' => <integer>,
                    'hostPort' => <integer>,
                    'protocol' => 'tcp|udp',
                ],
                // ...
            ],
            'privileged' => true || false,
            'readonlyRootFilesystem' => true || false,
            'ulimits' => [
                [
                    'hardLimit' => <integer>, // REQUIRED
                    'name' => 'core|cpu|data|fsize|locks|memlock|msgqueue|nice|nofile|nproc|rss|rtprio|rttime|sigpending|stack', // REQUIRED
                    'softLimit' => <integer>, // REQUIRED
                ],
                // ...
            ],
            'user' => '<string>',
            'volumesFrom' => [
                [
                    'readOnly' => true || false,
                    'sourceContainer' => '<string>',
                ],
                // ...
            ],
            'workingDirectory' => '<string>',
        ],
        // ...
    ],
    'cpu' => '<string>',
    'executionRoleArn' => '<string>',
    'family' => '<string>', // REQUIRED
    'memory' => '<string>',
    'networkMode' => 'bridge|host|awsvpc|none',
    'placementConstraints' => [
        [
            'expression' => '<string>',
            'type' => 'memberOf',
        ],
        // ...
    ],
    'requiresCompatibilities' => ['<string>', ...],
    'taskRoleArn' => '<string>',
    'volumes' => [
        [
            'host' => [
                'sourcePath' => '<string>',
            ],
            'name' => '<string>',
        ],
        // ...
    ],
]);


###TODO runtask
###TODO Pick an image from lexo registry, pull it, run it

$result = $client->runTask([
    'cluster' => '<string>',
    'count' => <integer>,
    'group' => '<string>',
    'launchType' => 'EC2|FARGATE',
    'networkConfiguration' => [
        'awsvpcConfiguration' => [
            'assignPublicIp' => 'ENABLED|DISABLED',
            'securityGroups' => ['<string>', ...],
            'subnets' => ['<string>', ...], // REQUIRED
        ],
    ],
    'overrides' => [
        'containerOverrides' => [
            [
                'command' => ['<string>', ...],
                'cpu' => <integer>,
                'environment' => [
                    [
                        'name' => '<string>',
                        'value' => '<string>',
                    ],
                    // ...
                ],
                'memory' => <integer>,
                'memoryReservation' => <integer>,
                'name' => '<string>',
            ],
            // ...
        ],
        'executionRoleArn' => '<string>',
        'taskRoleArn' => '<string>',
    ],
    'placementConstraints' => [
        [
            'expression' => '<string>',
            'type' => 'distinctInstance|memberOf',
        ],
        // ...
    ],
    'placementStrategy' => [
        [
            'field' => '<string>',
            'type' => 'random|spread|binpack',
        ],
        // ...
    ],
    'platformVersion' => '<string>',
    'startedBy' => '<string>',
    'taskDefinition' => '<string>', // REQUIRED
]);



###TODO Stop after x seconds or when button clicked
$result = $client->stopTask([/* ... */]);


